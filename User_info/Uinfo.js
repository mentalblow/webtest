document.getElementById("S_menu").addEventListener("click", Jssubmenu);
document.getElementById("nav-user1").addEventListener("click", TABS1);
document.getElementById("nav-user2").addEventListener("click", TABS2);
document.getElementById("nav-user3").addEventListener("click", TABS3);

$(".sub-menu ul").hide();
$(".profile-reviewtab").hide();
$(".profile-Posttab").hide();

function Jssubmenu(){
      $(this).parent(".sub-menu").children("ul").slideToggle("200");
      $(this).find("i.fa").toggleClass("fa-angle-up fa-angle-down");
}

function TABS1(){
    $(".profile-posttab").show();
    $(".profile-reviewtab").hide();
    $(".profile-Posttab").hide();
    
}

function TABS2(){
    $(".profile-posttab").hide();
    $(".profile-reviewtab").show();
    $(".profile-Posttab").hide();
    
}

function TABS3(){
    $(".profile-posttab").hide();
    $(".profile-reviewtab").hide();
    $(".profile-Posttab").show();
    
}