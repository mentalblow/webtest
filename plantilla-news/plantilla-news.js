const defaultBTn = document.getElementById("default-btn");
const defaultBTn2 = document.getElementById("default-btn2");
const defaultBTn3 = document.getElementById("default-btn3");
const defaultBTn4 = document.getElementById("default-btn4");
const FileName1 = document.querySelector(".file-name1");
const FileName2 = document.querySelector(".file-name2");
const FileName3 = document.querySelector(".file-name3");
const img1 = document.getElementById("img1");
const img2 = document.getElementById("img2");
const img3 = document.getElementById("img3");
const vid = document.getElementById("UVideo");

document.getElementById("S_menu").addEventListener("click", Jssubmenu);
document.getElementById("custom-btn1").addEventListener("click", DefaultBtnActive1);
document.getElementById("custom-btn2").addEventListener("click", DefaultBtnActive2);
document.getElementById("custom-btn3").addEventListener("click", DefaultBtnActive3);
document.getElementById("vid-btn").addEventListener("click", DefaultBtnActiveVid);
document.getElementById("custom-btn4").addEventListener("click", vidBtnActive);

$(".sub-menu ul").hide();
$(".profile-reviewtab").hide();
$(".profile-Posttab").hide();

function Jssubmenu(){
      $(this).parent(".sub-menu").children("ul").slideToggle("200");
      $(this).find("i.fa").toggleClass("fa-angle-up fa-angle-down");
}

function DefaultBtnActive1(){
      defaultBTn.click();

      defaultBTn.addEventListener("change",function(){
            const file = this.files[0];
            if(file){
                  const reader1 = new FileReader()
                  reader1.onload = function(){
                        const result = reader1.result;
                        img1.src = result;
                        img1.style.visibility = 'visible';
                  }
                  reader1.readAsDataURL(file);
            }
            if(this.value){
                  let valueStore = this.value;
                  fileName1.textContent = valueStore;
            }
      });
}

function DefaultBtnActive2(){
      defaultBTn2.click();

      defaultBTn2.addEventListener("change",function(){
            const file = this.files[0];
            if(file){
                  const reader2 = new FileReader()
                  reader2.onload = function(){
                        const result = reader2.result;
                        img2.src = result;
                        img2.style.visibility = 'visible';
                  }
                  reader2.readAsDataURL(file);
            }
            if(this.value){
                  let valueStore = this.value;
                  fileName2.textContent = valueStore;
            }
      });
}

function DefaultBtnActive3(){
      defaultBTn3.click();

      defaultBTn3.addEventListener("change",function(){
            const file = this.files[0];
            if(file){
                  const reader3 = new FileReader()
                  reader3.onload = function(){
                        const result = reader3.result;
                        img3.src = result;
                        img3.style.visibility = 'visible';
                  }
                  reader3.readAsDataURL(file);
            }
            if(this.value){
                  let valueStore = this.value;
                  fileName3.textContent = valueStore;
            }
      });
}

function DefaultBtnActiveVid(){
     var str = document.getElementById("TexVid").value;
     vid.src = str;
}

function vidBtnActive(){
      defaultBTn4.click();

      defaultBTn4.addEventListener("change",function(){
            const file = this.files[0];
            if(file){
                  const reader4 = new FileReader()
                  reader4.onload = function(){
                        const result = reader4.result;
                        vid.src = result;
                  }
                  reader4.readAsDataURL(file);
            }
      });
}